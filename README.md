[![CircleCI](https://circleci.com/bb/rambrogi/path-finding.svg?style=svg)](https://circleci.com/bb/rambrogi/path-finding)
## Getting Started

```binbash
$ git clone https://rambrogi@bitbucket.org/rambrogi/path-finding.git
$ cd path-finding && npm install
```

### Running in development mode:

```binbash
$ npm start
```

##### A browser tab/window should be opened. If not, open `http://localhost:3000/` on the browser.

### Running in production mode:

```binbash
$ npm run build
$ yarn global add serve && serve -s build
```

##### Open `http://localhost:5000/` on the browser.

### Running the tests:

```binbash
$ npm run test
```