import Grid from './grid';

describe('Grid with valid inputs', () => {
    const grid = new Grid(64);

    it('should initialize', () => {
        expect(grid.size).toEqual(64);
    });
    it('should accept starting position', () => {
        grid.setStart(10);
        expect(grid.start.value).toEqual(10);
    });
    it('should accept target position', () => {
        grid.setTarget(14);
        expect(grid.end.value).toEqual(14);
    });
    it('should map through each tile', () => {
        const arr = grid.map(item => item);
        expect(arr).toHaveLength(64);
    });
    it('should be able to solve', () => {
        grid.solver();
        expect(grid.solved).toBeTruthy();
        expect(grid.message).toEqual('Game ended! Refresh to play again!');
    });
});

describe('Grid with unreachable target', () => {
    const grid = new Grid(64);

    it('should initialize', () => {
        expect(grid.size).toEqual(64);
    });
    it('should accept starting position', () => {
        grid.setStart(10);
        expect(grid.start.value).toEqual(10);
    });
    it('should accept target position', () => {
        grid.setTarget(1);
        expect(grid.end.value).toEqual(1);
    });
    it('should accept an obstacle', () => {
        grid.setObstacle({ value: 2, obstacle: 'rock' });
        grid.setObstacle({ value: 9, obstacle: 'rock' });
        const tile = grid._getAt(2);
        expect(tile && tile.obstacle).toEqual('rock');
    });
    it('should fail to find a path', () => {
        grid.solver();
        expect(grid.message).toEqual('Failed to find a valid path!');
        expect(grid.solved).toBeFalsy();
    });
});

describe('Grid with wormholes', () => {
    it('should succeed with wormholes', () => {
        const grid = new Grid(64);
        grid.setStart(20);
        grid.setTarget(1);
        grid.setObstacle({ value: 3, obstacle: 'rock' });
        grid.setObstacle({ value: 9, obstacle: 'rock' });
        grid.setObstacle({ value: 10, obstacle: 'rock' });
        grid.setObstacle({ value: 11, obstacle: 'entrance'});
        grid.setObstacle({ value: 2, obstacle: 'exit'});
        grid.solver();
        expect(grid.solved).toBeTruthy();
        expect(grid.message).toEqual('Game ended! Refresh to play again!');
    });
});