class Tile implements TileInterface {
    value: number;
    left: TileInterface | null;
    top: TileInterface | null;
    right: TileInterface | null;
    bottom: TileInterface | null;
    hasPlayer: boolean;
    hasTreasure: boolean;
    obstacle: string;
    visited: boolean;
    cost: number;

    constructor({
        value,
        right = null,
        bottom = null,
    }: TileInterface) {
        this.right = right;
        this.bottom = bottom;
        this.hasPlayer = false;
        this.obstacle = '';
        this.value = value;
    }
}

class GridList implements GridInterface {
    gridMap: Map<number, TileInterface>;
    size: number;
    sizeSqrt: number;
    length: number;
    count: number;
    speed: number;
    row: number;
    isEdge: boolean;
    head: TileInterface;
    start: TileInterface;
    end: TileInterface;
    exits: Set<TileInterface>;
    paths: TileInterface[];
    currentNode: TileInterface;
    solved: boolean;
    message: string;
    _getAt: (value: number) => TileInterface | undefined;

    constructor(size: number) {
        this.gridMap = new Map();
        this.size = size;
        this.sizeSqrt = Math.sqrt(size);
        this.length = size;
        this.count = 1;
        this.speed = Math.sqrt(size);
        this.row = 0;
        this.exits = new Set();
        this.paths = [];
        this.solved = false;
        this.message = 'Click anywhere on the grid to choose the starting position';
        this._getAt = this.getAt;

        while (this.length !== 0) {
            this.addTile();
            this.length--;
        }
    }

    /**
     * Defines where the player will start in the grid.
     *
     * @param {number} value - The grid position number
     * @returns {(TileInterface | void)} - returns the updated node when valid.
     * @memberof GridList
     */
    setStart(value: number): TileInterface | void {
        const node = this.getAt(value);
        this.message = 'Click anywhere on the grid to choose the target position';

        if (node) {
            this.start = node;
            this.paths = [node];
            node.hasPlayer = true;
            return node;
        }
    }

    /**
     * Defines the target position in the grid.
     *
     * @param {number} value - The grid position number
     * @returns {(TileInterface | { error: string } | void)} - returns the updated node when valid.
     * @memberof GridList
     */
    setTarget(value: number): TileInterface | { error: string } | void {
        const node = this.getAt(value);
        this.message = 'Choose the desired obstacle on the left, and click on the map to place them';

        if (node) {
            const err = 'Can\'t place treasure there!!!';
            const targetNode = this.setTargetNode(node);

            return this.checkIfTileIsValid(!!node.hasPlayer, err, targetNode);
        }
    }

    /**
     * Defines the obstacle position in the grid.
     *
     * @param {{ value: number, obstacle: string}} { value, obstacle} - The clicked position and the obstacle type
     * @returns {(TileInterface | { error: string } | void)} - Updated node or an error or nothing.
     * @memberof GridList
     */
    setObstacle({ value, obstacle}: { value: number, obstacle: string}): TileInterface | { error: string } | void {
        const node = this.getAt(value);

        if (node) {
            const err = 'Can\'t place obstacle there!!!';

            const isInvalid = node.hasPlayer || node.hasTreasure;
            const obstacleNode = this.setObstacleNode(node, obstacle);

            return this.checkIfTileIsValid(!!isInvalid, err, obstacleNode);
        }
    }

    /**
     * Map through every tile and apply the callback to them.
     *
     * @param {function} - The callback function
     * @return {any} - any TileInterface item returned from the callback
     * @memberof GridList
     */
    map = (cb: (item: TileInterface) => any ) => { // tslint:disable-line
        const arr: TileInterface[] = [];
        this.gridMap.forEach(item => {
            arr.push(cb(item));
        });
        return arr.reverse();
    }

    /**
     * tries to find the shortest path. or show error message if no path is found.
     *
     * @memberof GridList
     */
    solver() {
        const checkIfWalkable = (node: TileInterface) => node.obstacle !== 'rock' && !node.visited;

        while (this.paths.length > 0) {
            const node = this.paths.shift();

            if (node) {
                this.currentNode = node;

                this.currentNode.cost = this.currentNode.cost || 1;
                this.checkForTreasure();

                this.currentNode.visited = true;
                const nodes = this.getConnectedNodes();

                for (const n of nodes) {
                   this.checkConnectedNode(n, checkIfWalkable(n));
                }
            }
        }

        if (this.paths.length === 0 && !this.solved) {
            this.message = 'Failed to find a valid path!';
        }
    }

    //////////////////////////////////
    // GRID CREATOR PRIVATE METHODS //
    /////////////////////////////////

    /**
     * returns the tile below the current one.
     *
     * @private
     * @returns {(TileInterface | null} - The bottom tile if any.
     * @memberof GridList
     */
    private getBottomTile() {
        const position = this.length + this.speed;
        const tile = this.gridMap.get(position);
        return this.row > 0 ? tile : null;
    }

    /**
     * returns the tile on the right of the current one.
     *
     * @private
     * @returns {(TileInterface | null} - The right tile if any.
     * @memberof GridList
     */
    private getRightTile(isEdge: boolean) {
        return isEdge ? null : this.head;
    }

    /**
     * Increase row number when edge is reached.
     *
     * @private
     * @param {boolean} isEdge - If Edge, increase row number by one.
     * @memberof GridList
     */
    private increaseRow(isEdge: boolean) {
        this.row = isEdge ? this.row + 1 : this.row;
    }

    /**
     * create the tiles for the grid. starting from the bottom right to
     * the top left.
     *
     * @private
     * @memberof GridList
     */
    private insertTile() {
        const isEdge = this.count % this.sizeSqrt === 0;
        const headIsOnEdge = (val: number) => (val - 1) % this.sizeSqrt === 0;

        this.increaseRow(isEdge);

        let bottomTile = this.getBottomTile();

        const tile = new Tile({
            value: this.length,
            right: this.getRightTile(isEdge),
            visited: false,
            cost: 1,
            left: null,
            top: null,
            bottom: bottomTile || null
        });

        if (bottomTile) {
            bottomTile.top = tile;
            tile.bottom = bottomTile;
        }

        this.head.left = headIsOnEdge(this.head.value) ? null : tile;
        this.head = tile;

        this.gridMap.set(this.length, this.head);

        this.count++;
    }

    /**
     * the very first tile
     *
     * @private
     * @memberof GridList
     */
    private insertFirst() {
        this.head = new Tile({
            value: this.length,
            visited: false,
            cost: 1,
            top: null,
            left: null,
            bottom: null,
            right: null
        });

        this.gridMap.set(this.length, this.head);
    }

    /////////////////////////////
    //  TILES PRIVATE METHODS //
    ////////////////////////////

    /**
     * check if there is a head tile. if yes,
     * continue adding a new one. if no, create it.
     *
     * @private
     * @memberof GridList
     */
    private addTile() {
        this.head ?
            this.insertTile() :
            this.insertFirst();
    }

    /**
     * get tile at the given position.
     *
     * @private
     * @param {number} value - The position in the grid
     * @returns {TileInterface} - The tile at the position
     * @memberof GridList
     */
    private getAt(value: number) {
        return this.gridMap.get(value);
    }

    /**
     * return the tile if the given criteria passes. otherwise,
     * returns the given error message.
     *
     * @private
     * @param {boolean} isValid - The criteria
     * @param {string} err - The error message
     * @param {TileInterface} node - The tile
     * @returns {object | TileInterface} - The error object or the tile.
     * @memberof GridList
     */
    private checkIfTileIsValid(isValid: boolean, err: string, node: TileInterface) {
        return isValid ? {error: err} : node;
    }

    /**
     * check if the current node is a wormhole exit.
     * if yes, add it to the 'exits' array. otherwise remove it
     * from there if any.
     *
     * @private
     * @param {TileInterface} n - The current node;
     * @param {string} type - The obstacle type;
     * @memberof GridList
     */
    private checkObstacleType(n: TileInterface, type: string) {
        type === 'exit' ?
            this.exits.add(n) :
            this.exits.delete(n);
    }

    /**
     * set the target position
     *
     * @private
     * @param {TileInterface} node - The tile with the 'treasure'
     * @returns {TileInterface} - The updated tile;
     * @memberof GridList
     */
    private setTargetNode(node: TileInterface) {
        this.end = node;
        node.hasTreasure = true;
        return node;
    }

    /**
     * updates the node with an obstacle
     *
     * @private
     * @param {TileInterface} node - The given tile
     * @param {string} obstacle - The obstacle type
     * @returns {TileInterface} - The updated tile.
     * @memberof GridList
     */
    private setObstacleNode(node: TileInterface, obstacle: string) {
        node.obstacle = obstacle;
        this.checkObstacleType(node, obstacle);

        node.cost = obstacle === 'dirt' ? 2 : 1;

        return node;
    }

    /////////////////////////////
    // SOLVER PRIVATE METHODS //
    ////////////////////////////

    /**
     * check if current node have treasure
     * if yes, update items.
     *
     * @private
     * @memberof GridList
     */
    private checkForTreasure() {
        if (this.currentNode && this.currentNode.hasTreasure) {
            this.message = 'Game ended! Refresh to play again!';
            let from = this.currentNode.from;
            this.start.hasPlayer = false;
            this.currentNode.hasPlayer = true;
            this.solved = true;

            while (from) {
                from.showPath = true;
                from = from.from;
            }
        }
    }

    /**
     * check if connected nodes are valid and exist.
     *
     * @private
     * @returns {TileInterface[]} - The connected nodes
     * @memberof GridList
     */
    private getValidNodes(): TileInterface[] {
        const { top, right, bottom, left } = this.currentNode;
        const allNodes = [top, right, bottom, left];
        const nodes: TileInterface[] = [];
        for (const node of allNodes) {
            if (node) {
                nodes.push(node);
            }
        }
        return nodes;
    }

    /**
     * sort connected nodes by cost
     *
     * @private
     * @param {number} valueA -  First node to compare
     * @param {number} valueB - Second node to compare
     * @param {number} sum - the sum of the accumulated cost
     * @returns {number} - -1 or 1 for .sort()
     * @memberof GridList
     */
    private checkSortSum(valueA: number, valueB: number, sum: number): number {
        const aIsCloser = this.end.value - valueA < this.end.value - valueB;
        return aIsCloser ? -1 : 1;
    }

    /**
     * get connected nodes of the current one.
     *
     * @private
     * @returns {array} - Sorted nodes.
     * @memberof GridList
     */
    private getConnectedNodes() {
        const nodes = this.getValidNodes();
        const getCost = ({ cost = 1 }: TileInterface) => cost;

        return nodes.sort((a, b) => {
            const costA = getCost(a);
            const costB = getCost(b);
            const sum = costA - costB;
            return sum === 0 ?
                this.checkSortSum(a.value, b.value, sum) :
                costA - costB;
        });
    }

    /**
     * check if adjacent node is an entrance wormhole.
     *
     * @private
     * @param {TileInterface} tile - The current tile
     * @memberof GridList
     */
    private checkForPortal(tile: TileInterface) {
        if (tile.obstacle === 'entrance') {
            this.exits.forEach(exit => {
                exit.from = tile;
                this.paths.push(exit);
            });
        }
    }

    /**
     * check if connected node is walkable.
     * if yes, update it accordingly
     *
     * @private
     * @param {TileInterface} node - The connected node
     * @param {boolean} tileIsWalkable - If is walkable or not
     * @memberof GridList
     */
    private checkConnectedNode(node: TileInterface, tileIsWalkable: boolean) {
        if (node && tileIsWalkable) {
            node.visited = true;
            node.cost = node.cost || 1;
            node.cost = this.currentNode.cost + node.cost;
            this.paths.push(node);
            node.from = this.currentNode;
            this.checkForPortal(node);
        }
    }
}

export default GridList;