import * as React from 'react';
import styled from 'styled-components';

import GridCreator from '../utils/grid';
import Grid from './Grid';
import GridItems from './GridItems';

const GRID_SIZE = 64;
const grid = new GridCreator(GRID_SIZE);

const MainWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;
    width: 100vw;
    height: 100vh;
`;

class App extends React.Component {
    state = {
        item: 'pirate',
        step: 1
    };

    solveCourse = (): void | string => {
        if (this.state.step >= 3) {
            grid.solver();
            this.forceUpdate();
        } else {
           return console.warn('Start/Target positions are not set');
        }
    }

    getSteps(item: TileInterface) {
        const steps = {
            1: {
                fn: () => grid.setStart(item.value),
                nextItem: 'treasure'
            },
            2: {
                fn: () => grid.setTarget(item.value),
                nextItem: 'entrance'
            },
            3: {
                fn: () => grid.setObstacle({ value: item.value, obstacle: this.state.item }),
                nextItem: this.state.item
            }
        };

        return steps[this.state.step];
    }

    handleClick = (item: TileInterface) => {
        const { step } = this.state;
        const currStep = this.getSteps(item);
        const handleNextStep = currStep.fn();
        const nextStep = step === 3 ? step : step + 1;

        handleNextStep.error ?
            console.warn(handleNextStep.error) :
            this.setState({ step: nextStep, item: currStep.nextItem });
    }

    handleObstaclesClick = (item: string) => {
        this.setState({ item });
    }

    render() {
        return (
            <MainWrapper>
                <GridItems
                    onClick={this.handleObstaclesClick}
                    selectedItem={this.state.item}
                    step={this.state.step}
                />
                <Grid
                    grid={grid}
                    selectedItem={this.state.item}
                    onClick={this.handleClick}
                    solve={this.solveCourse}
                    step={this.state.step}
                />
            </MainWrapper>
        );
    }
}

export default App;
