import * as React from 'react';
import { shallow } from 'enzyme';

import config from '../../setupTests';
import Item from './index';

config();

describe('Main Component', () => {
    it('should render without throwing an error', () => {
        const wrapper = shallow(<Item enabled={true} name="pirate" selected={true} onClick={jest.fn()} />);

        expect(wrapper).toMatchSnapshot();
    });
});
