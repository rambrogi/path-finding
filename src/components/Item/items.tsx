import * as React from 'react';
import styled, { keyframes } from 'styled-components';

const pirate = require('../../assets/pirate.png');
const treasure = require('../../assets/treasure.png');
const dirt = require('../../assets/dirt.jpg');
const portalBlue = require('../../assets/portal_blue.png');
const portalOrange = require('../../assets/portal_orange.png');
const rock = require('../../assets/rock.png');

const pulse = keyframes`
    0% {
        transform: scale(1);
    }
    50% {
        transform: scale(1.2);
    }
    100% {
        transform: scale(1);
    }
`;

const Asset = styled.span`
    width: 75px;
    height: 75px;
    z-index: 1;
    display: block;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center bottom;
    ${(props: { enabled: boolean; selected: boolean }) => `
    cursor: ${props.enabled ? 'pointer' : 'not-allowed'};
        opacity: ${props.enabled ? '1' : '0.2'};
        animation: ${props.selected && `${pulse} 1.4s linear infinite`};
    `};
`;

const Pirate = Asset.extend`
    background-image: url('${pirate}');
    background-size: 90px;
    background-position: -8px -20px;
    z-index: 3;
`;

const Treasure = Asset.extend`
    background-image: url('${treasure}');
`;

const Dirt = Asset.extend`
    background-image: url('${dirt}');
`;

const PortalEntrance = Asset.extend`
    background-image: url('${portalOrange}');
    z-index: 4;
`;

const PortalExit = Asset.extend`
    background-image: url('${portalBlue}');
    z-index: 4;
`;

const Rock = Asset.extend`
    background-image: url('${rock}');
`;

export default ({
    isSelected = true,
    enabled = true,
    onClick
}: {
    isSelected?: boolean;
    enabled?: boolean;
    onClick?: (item: string) => void;
}) => ({
    pirate: (
        <Pirate
            enabled={enabled}
            selected={isSelected}
            title="Starting Location"
        />
    ),
    treasure: (
        <Treasure
            enabled={enabled}
            selected={isSelected}
            title="Target Location"
        />
    ),
    dirt: (
        <Dirt
            onClick={() => onClick ? onClick('dirt') : null}
            enabled={enabled}
            selected={isSelected}
            title="Gravel: half of the regular speed"
        />
    ),
    entrance: (
        <PortalEntrance
            onClick={() => onClick ? onClick('entrance') : null}
            enabled={enabled}
            selected={isSelected}
            title="Wormhole Entrance: travel to instantaneously to any exit"
        />
    ),
    exit: (
        <PortalExit
            onClick={() => onClick ? onClick('exit') : null}
            enabled={enabled}
            selected={isSelected}
            title="Wormhole Exit: can be reached by any wormhole entrance"
        />
    ),
    rock: (
        <Rock
            onClick={() => onClick ? onClick('rock') : null}
            enabled={enabled}
            selected={isSelected}
            title="Boulder: there is no way to travel through boulders"
        />
    )
});
