import * as React from 'react';
import items from './items';

class Item extends React.Component {
    props: {
        enabled?: boolean;
        name: string;
        selected?: string;
        onClick?: (item: string) => void;
    };

    getItems(): JSX.Element {
        const { enabled, name, selected, onClick } = this.props;
        const isSelected = name === selected;

        return items({ enabled, isSelected, onClick })[name];
    }

    render() {
        return this.getItems();
    }
}

export default Item;
