import * as React from 'react';
import styled from 'styled-components';

import Item from '../Item';

const Wrapper = styled.div`
    margin-right: 10px;
    border: 1px solid gray;
    padding: 5px;
    border-radius: 10px;
    background-color: #efefef;
    > span {
        margin: 15px 0;
    }
`;

class GridItems extends React.Component {
    props: {
        selectedItem: string;
        step: number;
        onClick: (item: string) => void;
    };

    render() {
        const { step, selectedItem } = this.props;
        const items = [
            { name: 'pirate', step: 1 },
            { name: 'treasure', step: 2 },
            { name: 'entrance', step: 3 },
            { name: 'exit', step: 3 },
            { name: 'rock', step: 3 },
            { name: 'dirt', step: 3 }
        ];
        return (
            <Wrapper>
                {items.map(item => (
                    <Item
                        onClick={() => this.props.onClick(item.name)}
                        key={item.name}
                        enabled={step === item.step}
                        name={item.name}
                        selected={selectedItem}
                    />
                ))}
            </Wrapper>
        );
    }
}

export default GridItems;
