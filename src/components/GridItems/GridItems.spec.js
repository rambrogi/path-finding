import * as React from 'react';
import { shallow } from 'enzyme';

import config from '../../setupTests';
import GridItems from './index';

config();

describe('Main Component', () => {
    it('should render without throwing an error', () => {
        const wrapper = shallow(<GridItems step={1} selectedItem='pirate' />);

        expect(wrapper).toMatchSnapshot();
    });
});
