import * as React from 'react';
import { shallow } from 'enzyme';
import config from '../setupTests';

config();

import App from './index';

describe('Main Component', () => {
    it('should render without throwing an error', () => {
        const wrapper = shallow(<App />);

        expect(wrapper.length).toEqual(1);
    });
    it('should have a solver', () => {
        const wrapper = shallow(<App />);
        wrapper.instance().handleClick({ value: 2, cost: 1 })
        wrapper.instance().handleClick({ value: 10, cost: 1 })
        wrapper.instance().handleObstaclesClick('dirt');
        wrapper.instance().handleClick({ value: 4, cost: 2 });
        wrapper.instance().solveCourse();
        expect(wrapper.length).toEqual(1);
    });
});
