import * as React from 'react';
import { shallow } from 'enzyme';

import config from '../../setupTests';
import Grid from './index';

config();

describe('Main Component', () => {
    it('should render without throwing an error', () => {
        const grid = {
            message: 'hi',
            size: 32,
            map: jest.fn()
        }
        const wrapper = shallow(<Grid grid={grid} onClick={jest.fn()} />);

        expect(wrapper).toMatchSnapshot();
    });
});
