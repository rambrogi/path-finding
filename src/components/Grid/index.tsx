import * as React from 'react';
import styled from 'styled-components';

import Item from '../Item';
const floor = require('../../assets/floor.jpg');

const Wrapper = styled.div`
    width: ${(props: { size: number }) => `calc(75px * ${Math.sqrt(props.size)})`};
`;

const GridWrapper = styled.div`
    display: flex;
    width: 100%;
    flex-wrap: wrap;
    margin: auto;
`;

const GridTile = styled.div`
    width: 75px;
    border: 1px solid black;
    height: 75px;
    box-sizing: border-box;
    background-image: url('${floor}');
    background-size: 145px;
    position: relative;
    > span {
        position: absolute;
        top: 0;
    }
`;

const Dot = styled.div`
    text-align: center;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 1;
    background-color: rgba(76, 54, 33, 0.38);
    box-sizing: border-box;
`;

const Msg = styled.p`
    width: 100%;
    text-align: center;
    font-size: 32px;
    font-weight: bold;
    color: white;
`;

const Button = styled.button`
    width: 100%;
    height: 75px;
    font-size: 24px;
    ${(props: {disabled: boolean}) => `
        background-color: ${props.disabled ? '#ececec' : '#d25648'};
        color: ${props.disabled ? '#dadada' : 'white'};
    `};
    border: none;
    outline: none;
`;

class Grid extends React.Component {
    props: {
        selectedItem: string;
        grid: GridInterface;
        onClick: (item: object, ev: object) => void;
        solve: () => void;
        step: number;
    };

    render() {
        const { message, size, map } = this.props.grid;
        return (
            <Wrapper size={size}>
                <Button disabled={this.props.step < 3} onClick={this.props.solve}>Solve</Button>
                <Msg>{message}</Msg>
                <GridWrapper>
                    {map((item: TileInterface): JSX.Element => (
                        <GridTile
                            key={item.value}
                            data-value={item.value}
                            onClick={(ev: object) => this.props.onClick(item, ev)}
                        >
                            {item.value}
                            {item.hasPlayer && <Item name="pirate"/>}
                            {item.hasTreasure && <Item name="treasure" />}
                            {item.obstacle && <Item name={item.obstacle} />}
                            {item.showPath && <Dot><span>🕳️</span></Dot>}
                        </GridTile>
                    ))}
                </GridWrapper>
            </Wrapper>
        );
    }
}

export default Grid;
