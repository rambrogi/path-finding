interface GridInterface {
    gridMap: Map<number, TileInterface>;
    size: number;
    sizeSqrt: number;
    length: number;
    count: number;
    speed: number;
    row: number;
    head: TileInterface;
    map: (cb: (item: TileInterface) => JSX.Element) => TileInterface[];
    message: string;
}