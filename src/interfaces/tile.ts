interface TileInterface {
    value: number;
    left: TileInterface | null;
    top: TileInterface | null;
    right: TileInterface | null;
    bottom: TileInterface | null;
    hasPlayer?: boolean;
    hasTreasure?: boolean;
    obstacle?: string;
    visited?: boolean;
    from?: TileInterface;
    showPath?: boolean;
    cost: number;
}